import React from 'react';
import {NavLink} from "react-router-dom";
import {Card, CardBody, CardImg, CardSubtitle, CardTitle} from "reactstrap";
import {apiURL} from "../../constants";

const Product = (props) => {
    return (
        <NavLink style={{color: "black", textDecoration: "none"}}
                 to={"/products/" + props.id} exact>
            <Card style={{marginBottom: "10px", textAlign: "center"}}>
                <CardBody>
                        <CardImg top width="100%" alt="img"
                                 src={apiURL + '/uploads/' + props.image}
                        />
                    <CardTitle style={{
                        fontSize: "25px",
                        fontWeight: "bold",
                        margin: "10px 0 0"
                    }}>
                        {props.title}
                    </CardTitle>

                    <CardSubtitle>
                        {props.price} som
                    </CardSubtitle>

                </CardBody>
            </Card>
        </NavLink>
    );
};

export default Product;