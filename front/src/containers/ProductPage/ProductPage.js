import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteProduct, fetchProduct} from "../../store/actions/productsActions";
import {Button, Card, CardBody, CardFooter, CardImg, CardSubtitle, CardTitle} from "reactstrap";
import {apiURL} from "../../constants";
import Spinner from "../../components/UI/Spinner/Spinner";

class ProductPage extends Component {
    componentDidMount() {
        this.props.fetchProduct(this.props.match.params.id);
    }

    render() {
        return (
            this.props.loading ? <Spinner/> :
                this.props.product ?
                    <Card style={{marginBottom: "10px"}}>
                        <CardBody style={{padding: "0"}}>
                            <div style={{textAlign: "center"}}>
                                <CardImg style={{width: "300px", textAlign: "center"}} alt="img"
                                         src={apiURL + '/uploads/' + this.props.product.image}
                                />
                                <CardSubtitle>
                                    {this.props.product.category.title}
                                </CardSubtitle>
                            </div>
                            <div style={{marginLeft: "15px"}}>
                                <CardTitle style={{
                                    fontSize: "25px",
                                    fontWeight: "bold",
                                    margin: "10px 0 0"
                                }}>
                                    {this.props.product.title}
                                </CardTitle>
                                <CardSubtitle>
                                    {this.props.product.price} som
                                </CardSubtitle>
                                <CardSubtitle>
                                    {this.props.product.description}
                                </CardSubtitle>
                            </div>
                            <CardFooter className="text-muted" style={{padding:"30px"}}>
                                <span style={{marginRight: "10px"}}>{this.props.product.user.displayName}</span>
                                <span>{this.props.product.user.phoneNumber}</span>
                                {this.props.user && this.props.product.user._id === this.props.user._id?
                                    <Button style={{float:"right"}} onClick={()=>this.props.deleteProduct(this.props.product._id)}>Delete</Button>:null}
                            </CardFooter>
                        </CardBody>
                    </Card> : <h3>No product</h3>
        );
    }
}

const mapStateToProps = state => ({
    loading: state.products.loading,
    product: state.products.product,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchProduct: id => dispatch(fetchProduct(id)),
    deleteProduct: id => dispatch(deleteProduct(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductPage);
