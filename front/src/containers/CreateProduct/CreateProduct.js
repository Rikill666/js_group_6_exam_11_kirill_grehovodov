import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormFeedback, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../../components/UI/FormElement/FormElement";
import {createProduct} from "../../store/actions/productsActions";
import {fetchCategories} from "../../store/actions/categoriesActions";

class CreateProduct extends Component {
    componentDidMount() {
        if (!this.props.user)
            this.props.history.push("/login");
        this.props.fetchCategories();
    };

    state = {
        title: "",
        description: "",
        image: "",
        price: "",
        category: ""
    };
    inputChangeHandler = (event) => {
        this.setState({[event.target.name]: event.target.value});
    };
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };
    submitFormHandler = async event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        await this.props.createProduct(formData);
    };
    getFieldError = fieldName => {
        try {
            if (fieldName === "category" && this.props.error.errors[fieldName].message) {
                return "Path `category` is required."
            }
            return this.props.error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    render() {
        return (
            <>
                <h2>New post: </h2>
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        type="text"
                        propertyName="title"
                        title="Title"
                        value={this.state.title}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('title')}
                        placeholder='Enter title'
                        autoComplete='new-title'
                    />
                    <FormElement
                        type="number"
                        propertyName="price"
                        title="Price"
                        value={this.state.price}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('price')}
                        placeholder='Enter price'
                        autoComplete='new-price'
                    />
                    <FormGroup row>
                        <Label sm={2} for="category">Category</Label>
                        <Col sm={10}>
                            <Input
                                invalid={!!this.getFieldError('category')}
                                type="select"
                                name="category" id="category"
                                value={this.state.category}
                                onChange={this.inputChangeHandler}
                            >
                                <option value="">Please select a category</option>
                                {this.props.categories.map(category => (
                                    <option key={category._id} value={category._id}>{category.title}</option>
                                ))}
                            </Input>
                            <FormFeedback>{this.getFieldError('category')}</FormFeedback>
                        </Col>
                    </FormGroup>
                    <FormElement
                        type="text"
                        propertyName="description"
                        title="Description"
                        value={this.state.description}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('description')}
                        placeholder='Enter description'
                        autoComplete='new-description'
                    />
                    <FormGroup row>
                        <Label sm={2} for="image">Image</Label>
                        <Col sm={10}>
                            <Input
                                invalid={!!this.getFieldError('image')}
                                type="file"
                                name="image" id="image"
                                onChange={this.fileChangeHandler}
                            />
                            <FormFeedback>{this.getFieldError('image')}</FormFeedback>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">
                                Create
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        error: state.products.createError,
        categories: state.categories.categories
    };
};

const mapDispatchToProps = dispatch => {
    return {
        createProduct: (productData) => dispatch(createProduct(productData)),
        fetchCategories: () => dispatch(fetchCategories()),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(CreateProduct);