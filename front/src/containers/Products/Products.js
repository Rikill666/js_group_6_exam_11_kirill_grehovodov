import React, {Component} from 'react';
import {Col, Row} from "reactstrap";
import Product from "../../components/Product/Product";
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {fetchCategories} from "../../store/actions/categoriesActions";
import {fetchProducts} from "../../store/actions/productsActions";
import Spinner from "../../components/UI/Spinner/Spinner";

class Products extends Component {
   async componentDidMount() {
       await this.props.fetchCategories();
       await this.props.fetchProducts();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.match.params.id !== this.props.match.params.id){
            this.props.fetchProducts(this.props.match.params.id);
        }
    }

    render() {
        return (
            this.props.loading?<Spinner/>:
            <Row>
                <Col lg="3" md="4" xs="6">
                    {this.props.categories.length > 0 ?
                        <ul>
                            <li><NavLink to={'/categories'}>All categories</NavLink></li>
                            {this.props.categories.map(c => (
                            <li key={c._id}>
                                <NavLink to={'/categories/' + c._id}>{c.title}</NavLink>
                            </li>
                            ))}
                        </ul> : <h4>No categories</h4>}
                </Col>
                <Col lg="9" md="8" xs="6">
                    <Row>
                        {this.props.products.length>0 ? this.props.products.map(product => (
                            <Col lg="4" md="6" xs="12" key={product._id}>
                                <Product

                                    title={product.title}
                                    price={product.price}
                                    id={product._id}
                                    image={product.image}
                                />
                            </Col>
                        )) : <h1>No Quotes</h1>}
                    </Row>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    products:state.products.products,
    categories: state.categories.categories,
    loading:state.products.loading,
});

const mapDispatchToProps = dispatch => ({
    fetchProducts: categoryID => dispatch(fetchProducts(categoryID)),
    fetchCategories: () => dispatch(fetchCategories()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Products)
