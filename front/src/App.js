import React, {Component, Fragment} from 'react';
import Toolbar from "./components/UI/Toolbar/Toolbar";

import {Container} from "reactstrap";
import {Route, Switch} from "react-router-dom";

import Products from "./containers/Products/Products";
import CreateProduct from "./containers/CreateProduct/CreateProduct";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import ProductPage from "./containers/ProductPage/ProductPage";


class App extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Toolbar/>
                </header>
                <Container style={{marginTop: '20px'}}>
                    <Switch>
                        <Route path="/" exact component={Products}/>
                        <Route path="/products/:id" exact component={ProductPage}/>
                        <Route path="/register" exact component={Register}/>
                        <Route path="/login" exact component={Login}/>
                        <Route path="/categories" exact component={Products}/>
                        <Route path="/categories/:id" exact component={Products}/>
                        <Route path="/add_product" exact component={CreateProduct}/>
                        <Route render={() => <h1>Not Found</h1>}/>
                    </Switch>
                </Container>
            </Fragment>
        );
    }
}

export default App;