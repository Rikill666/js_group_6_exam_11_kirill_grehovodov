import axiosApi from "../../axiosApi";
import {push} from "connected-react-router";

export const FETCH_PRODUCTS_REQUEST = 'FETCH_PRODUCTS_REQUEST';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_ERROR = 'FETCH_PRODUCTS_ERROR';

export const CREATE_PRODUCT_REQUEST = 'CREATE_PRODUCT_REQUEST';
export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';
export const CREATE_PRODUCT_ERROR = 'CREATE_PRODUCT_ERROR';

export const DELETE_PRODUCT_REQUEST = 'DELETE_PRODUCT_REQUEST';
export const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS';
export const DELETE_PRODUCT_ERROR = 'DELETE_PRODUCT_ERROR';

export const FETCH_PRODUCT_REQUEST = 'FETCH_PRODUCT_REQUEST';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const FETCH_PRODUCT_ERROR = 'FETCH_PRODUCT_ERROR';

export const fetchProductsRequest = () => {
    return {type: FETCH_PRODUCTS_REQUEST};
};
export const fetchProductsSuccess = products => ({type: FETCH_PRODUCTS_SUCCESS, products});
export const fetchProductsError = (error) => {
    return {type: FETCH_PRODUCTS_ERROR, error};
};

export const createProductRequest = () => {
    return {type: CREATE_PRODUCT_REQUEST};
};
export const deleteProductRequest = () => {
    return {type: DELETE_PRODUCT_REQUEST};
};
export const deleteProductSuccess = () => ({type: DELETE_PRODUCT_SUCCESS});
export const deleteProductError = (error) => {
    return {type: DELETE_PRODUCT_ERROR, error};
};


export const createProductSuccess = () => ({type: CREATE_PRODUCT_SUCCESS});
export const createProductError = (error) => {
    return {type: CREATE_PRODUCT_ERROR, error};
};

export const fetchProductRequest = () => {
    return {type: FETCH_PRODUCT_REQUEST};
};
export const fetchProductSuccess = product => ({type: FETCH_PRODUCT_SUCCESS, product});
export const fetchProductError = (error) => {
    return {type: FETCH_PRODUCT_ERROR, error};
};

export const deleteProduct = (id) => {
    return async (dispatch,getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {"Authorization": "Token " + token};
            dispatch(deleteProductRequest());
            dispatch(createProductRequest());
            await axiosApi.delete('/products/' + id, {headers});
            dispatch(deleteProductSuccess());
            dispatch(push('/'));
        } catch (e) {
            dispatch(deleteProductError(e));
        }
    };
};

export const fetchProducts = (categoryID) => {
    return async dispatch => {
        try {
            dispatch(fetchProductsRequest());
            let response;
            if (categoryID) {
                response = await axiosApi.get('/products?category=' + categoryID);
            } else {
                response = await axiosApi.get('/products');
            }
            dispatch(fetchProductsSuccess(response.data));
        } catch (e) {
            dispatch(fetchProductsError(e));
        }
    };
};
export const createProduct = (productData) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {"Authorization": "Token " + token};
            dispatch(createProductRequest());
            await axiosApi.post('/products', productData, {headers});
            dispatch(createProductSuccess());
            dispatch(push('/'));
        } catch (error) {
            if (error.response) {
                dispatch(createProductError(error.response.data));
            } else {
                dispatch(createProductError({global: "Network error or no internet"}));
            }
        }
    };
};

export const fetchProduct = (id) => {
    return async dispatch => {
        try {
            dispatch(fetchProductRequest());
            const response = await axiosApi.get('/products/' + id);
            const post = response.data;
            dispatch(fetchProductSuccess(post));
        } catch (e) {
            dispatch(fetchProductError(e));
        }
    };
};
