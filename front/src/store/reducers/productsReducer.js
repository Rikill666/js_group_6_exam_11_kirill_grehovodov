import {
  CREATE_PRODUCT_ERROR,
  CREATE_PRODUCT_REQUEST, CREATE_PRODUCT_SUCCESS,
  FETCH_PRODUCT_ERROR,
  FETCH_PRODUCT_REQUEST,
  FETCH_PRODUCT_SUCCESS, FETCH_PRODUCTS_ERROR, FETCH_PRODUCTS_REQUEST,
  FETCH_PRODUCTS_SUCCESS
} from "../actions/productsActions";

const initialState = {
  products: [],
  product: null,
  loading:false,
  getError: null,
  createError:null,
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCT_REQUEST:
      return {...state};
    case FETCH_PRODUCT_SUCCESS:
      return {...state, product: action.product, error:null};
    case FETCH_PRODUCT_ERROR:
      return {...state, getError: action.error};

    case FETCH_PRODUCTS_REQUEST:
      return {...state, loading: true};
    case FETCH_PRODUCTS_SUCCESS:
      return {...state, products: action.products, getError:null, loading: false};
    case FETCH_PRODUCTS_ERROR:
      return {...state, getError: action.error, loading: false};

    case CREATE_PRODUCT_REQUEST:
      return {...state, loading: true};
    case CREATE_PRODUCT_SUCCESS:
      return {...state, createError:null, loading: false};
    case CREATE_PRODUCT_ERROR:
      return {...state, createError: action.error, loading: false};
    default:
      return state;
  }
};

export default productsReducer;