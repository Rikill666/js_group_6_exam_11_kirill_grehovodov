import {FETCH_CATEGORIES_ERROR, FETCH_CATEGORIES_REQUEST, FETCH_CATEGORIES_SUCCESS} from "../actions/categoriesActions";

const initialState = {
    categories: [],
    error:null
};

const categoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CATEGORIES_REQUEST:
            return {...state};
        case FETCH_CATEGORIES_SUCCESS:
            return {...state, error: null, categories: action.categories};
        case FETCH_CATEGORIES_ERROR:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default categoriesReducer;