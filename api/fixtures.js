const mongoose = require("mongoose");
const config = require('./config');
const Category = require('./models/Category');
const Product = require('./models/Product');
const User = require('./models/User');

const run = async () => {
    await mongoose.connect(config.database, config.databaseOptions);
    const collections = await mongoose.connection.db.listCollections().toArray();
    for (let coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }
    const [user, user1] = await User.create({
            username: 'admin',
            password: '123',
            token: 'l1Xqx4DD6sBhHXeNWxypN',
            displayName:"Admin",
            phoneNumber:"0555121314"
        },
        {
            username: 'user',
            password: '123',
            token: 'l1Xqx4DD6sBhHXeNWxyp6',
            displayName:"User",
            phoneNumber:"0705151819"
        });

    const [category1, category2, category3, category4] = await Category.create(
        {
            title: 'Computers',
        },
        {
            title: 'Cars',
        },
        {
            title: 'Phones',
        },
        {
            title: 'Coronavirus protection',
        },
    );

    await Product.create(
        {
            user: user,
            category: category1,
            image: 'fixtures/1.jpg',
            title: 'Pentium4',
            price: 2000,
            description: "complete garbage"
        },
        {
            user: user1,
            category: category1,
            image: 'fixtures/2.jpg',
            title: "Ноутбук Acer Aspire A315-41-R3YF",
            price: 27000,
            description: "Very good"
        },
        {
            user: user1,
            category: category2,
            image: 'fixtures/3.jpg',
            title: 'ГАЗ M-2 Победа',
            price: 5000,
            description: "ГАЗ М-20 Победа Год: 1953"
        },
        {
            user: user,
            category: category3,
            image: 'fixtures/4.jpg',
            title: "Смартфон Huawei Y7 2019 32GB Aurora Blue",
            price: 12890,
            description: "Широкоформатный 6,26-дюймовый дисплей с модным вырезом Dewdrop и разрешением HD+ (1520 x 720) обеспечивает Y7 2019 гораздо лучший обзор и еще больше места для контента. Он отличается высокой насыщенностью, четкостью и естественной передачей цветов, а также хорошей видимостью с любого угла. Взгляните по-новому на красочный мир у вас в руках."
        },
        {
            user: user,
            category: category3,
            image: 'fixtures/5.jpg',
            title: 'I-Phone 11',
            price: 50000,
            description: "iPhone 11 нельзя назвать абсолютно новым смартфоном, это прямой потомок iPhone XR. Смартфон изменился внешне очень минимально, габариты с предшественником совпадают полностью. Главным внешним новшеством являются обновленные цветовые решения, но это по-прежнему самый яркий iPhone. Еще одним изменением стало появление второго модуля камеры, теперь к широкоугольному добавился ультраширокоугольный объектив. Это позволит создавать групповые портреты и красивые панорамы. В качестве процессора теперь используется более быстрый и энергоэффективный Apple 13 Bionic. Что касается времени автономной работы, то и здесь компания сделала шаг вперед и смартфон должен работать как минимум на час дольше модели XR. "
        },
        {
            user: user,
            category: category4,
            image: 'fixtures/6.jpg',
            title: "Медицинская маска",
            price: 100,
            description: "Лучшая защита от вируса"
        },
        {
            user: user1,
            category: category4,
            image: 'fixtures/7.jpg',
            title: 'Перчатки',
            price: 35,
            description: "Держите руки в чистоте и не заразитесь сто пудов"
        },
        {
            user: user,
            category: category4,
            image: 'fixtures/8.jpg',
            title: "Бумага туалетная",
            price: 15,
            description: "С чистой жопой вирус не страшен"
        },
    );
};
run().catch(e => {
    mongoose.connection.close();
    throw e;
});