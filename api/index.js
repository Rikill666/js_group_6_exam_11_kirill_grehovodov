const express = require('express');
const cors = require('cors');
const products = require('./app/products');
const users = require('./app/users');
const categories = require('./app/categories');
const mongoose = require("mongoose");
const config = require('./config');

const app = express();
app.use(cors());
const port = 8000;

app.use(express.json());
app.use(express.static('public'));


const run = async () => {
    await mongoose.connect(config.database, config.databaseOptions);
    app.use('/products', products);
    app.use('/users', users);
    app.use('/categories', categories);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
};
run().catch(e => {
    console.log(e);
});


