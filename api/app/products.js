const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const config = require('../config');
const path = require('path');
const Product = require('../models/Product');
const Category = require('../models/Category');
const auth = require('../middleware/auth');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});
const router = express.Router();
const upload = multer({storage});

router.post('/', upload.single('image'), auth, async (req, res) => {
    const user = req.user;
    let img = "";
    if (req.file) {
        img = req.file.filename;
    }
    try {
        const product = new Product({
            user: user._id,
            category: req.body.category,
            title: req.body.title,
            price: req.body.price,
            description: req.body.description,
            image: img
        });
        await product.save();
        return res.send(product);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.get('/', async (req, res) => {
    let products;
    try {
        if (req.query.category) {
            const category = await Category.findOne({_id: req.query.category});
            if (!category) {
                return res.status(404).send("No this category");
            }
            products = await Product.find({category: req.query.category});

        } else {
            products = await Product.find();
        }
        return res.send(products);
    } catch (error) {
        return res.status(400).send(error);
    }
});
router.get('/:id', async (req, res) => {
    const productId = req.params.id;
    try {
        let product = await Product.findOne({_id: productId}).populate({path: 'user'}).populate({path: 'category'});
        if (!product) return res.status(400).send("Not found");
        return res.send(product);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.delete('/:id',auth, async (req, res) => {
    const user = req.user;
    const productId = req.params.id;
    try {
        const product = await Product.findOne({_id: productId});
        if(!product){
           return res.status(404).send("No this product");
        }

        if(user._id.toString() !== product.user.toString()){
            return res.status(403).send("You cannot delete this product");
        }

        await Product.deleteOne({_id: productId});
        return res.send("ok");
    } catch (error) {
        return res.status(400).send(error);
    }
});
module.exports = router;