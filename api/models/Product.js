const mongoose = require("mongoose");

const Scheme = mongoose.Schema;

const productScheme = new Scheme({
    user: {
        type: Scheme.Types.ObjectId,
        ref: 'User',
        required: true
    },
    category: {
        type: Scheme.Types.ObjectId,
        ref: 'Category',
        required: true,
    },
    title: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                if(value.length < 2) throw new Error('The name must be at least two characters long');
            }
        }
    },
    image: {
        type: String,
        required: true,

    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true,
        min: 1
    }
});

const Product = mongoose.model("Product", productScheme);
module.exports = Product;