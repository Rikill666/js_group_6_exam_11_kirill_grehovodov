const mongoose = require("mongoose");

const Scheme = mongoose.Schema;

const categoryScheme = new Scheme({
    title:{
        type: String,
        required: true
    },
});

const Category = mongoose.model("Category", categoryScheme);
module.exports = Category;